#! /usr/bin/env bash
# Installation script for Kafka 
# Licensed under the terms of GNU GPL-3+. See LICENSE at root.
# Copyright (C) 2021 Carlos Henrique Lima Melara <charlesmelara@outlook.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

if [[ ! -d /home/gcloud17/gcloud17 ]]; then
	echo "This script relies on gcloud17 repository being installed in user gcloud17 home"
	exit 1
fi

apt install nginx -y
mv /home/gcloud17/gcloud17/scripts/files/nginx.conf /etc/nginx/nginx.conf
systemctl restart nginx.service
