#! /usr/bin/env bash
# Installation script for Kafka 
# Licensed under the terms of GNU GPL-3+. See LICENSE at root.
# Copyright (C) 2021 Carlos Henrique Lima Melara <charlesmelara@outlook.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

if [[ ! -d /home/gcloud17/gcloud17 ]]; then
	echo "This script relies on gcloud17 repository being installed in user gcloud17 home"
	exit 1
fi

if [[ ! $(id -u kafka &>/dev/null) ]]; then
	echo 'Criando usuário kafka'
	adduser kafka
	adduser kafka sudo
	echo "kafka     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
fi

#su - kafka

if [[ ! -d /home/kafka/Downloads ]]; then
	sudo -u kafka mkdir /home/kafka/Downloads
fi

if [[ ! -f /home/kafka/Downloads/kafka.tgz ]]; then
	sudo -u kafka curl "https://downloads.apache.org/kafka/2.8.0/kafka_2.13-2.8.0.tgz" -o /home/kafka/Downloads/kafka.tgz
fi

if [[ ! -d /home/kafka/kafka ]]; then
	sudo -u kafka mkdir /home/kafka/kafka
	sudo -u kafka tar -xvf /home/kafka/Downloads/kafka.tgz --strip 1 --directory /home/kafka/kafka
	sudo -u kafka sed -ie 's/#listeners=PLAINTEXT:\/\/:9092/listeners=bugreport:\/\/10.1.0.21:9021/' /home/kafka/kafka/config/server.properties
	sudo -u kafka sed -ie 's/#listener.security.protocol.map=PLAINTEXT:PLAINTEXT,SSL:SSL,SASL_PLAINTEXT:SASL_PLAINTEXT,SASL_SSL:SASL_SSL/listener.security.protocol.map=PLAINTEXT:PLAINTEXT/' /home/kafka/kafka/config/server.properties
fi

if [[ ! -f /etc/systemd/system/kafka.service ]]; then
	cp /home/gcloud17/gcloud17/scripts/files/kafka.service /etc/systemd/system/kafka.service

	cp /home/gcloud17/gcloud17/scripts/files/zookeeper.service /etc/systemd/system/zookeeper.service
fi

systemctl start kafka
systemctl status kafka

systemctl enable zookeeper
systemctl enable kafka

deluser kafka sudo
passwd kafka -l
