#! /usr/bin/env bash
# Installation script for Kafka 
# Licensed under the terms of GNU GPL-3+. See LICENSE at root.
# Copyright (C) 2021 Carlos Henrique Lima Melara <charlesmelara@outlook.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 1
fi

if [[ ! -d /home/gcloud17/gcloud17 ]]; then
	echo "This script relies on gcloud17 repository being installed in user gcloud17 home"
	exit 1
fi

bash /home/gcloud17/gcloud17/scripts/install_kafka.sh
bash /home/gcloud17/gcloud17/scripts/install_nginx.sh

/home/kafka/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic bugs
/home/kafka/kafka/bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic comments

sudo -u gcloud17 screen -D -m -S butler-comments python3 /home/gcloud17/gcloud17/Butler/consumer_comment.py
sudo -u gcloud17 screen -D -m -S butler-bugs python3 /home/gcloud17/gcloud17/Butler/consumer_bug.py

sudo -u gcloud17 screen -D -m -S liftman python3 /home/gcloud17/gcloud17/Liftman/liftman.py

/home/gcloud17/gcloud17/BugPortal/node_modules/yarn/bin/yarn rw build
sudo -u gcloud17 screen -D -m -S bugportal /home/gcloud17/gcloud17/BugPortal/node_modules/yarn/bin/yarn rw serve
