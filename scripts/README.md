# Scripts de instalação

Esses scripts foram criados para automatizar a instalação do reportbug
na infraestrutura do lasdpc na USP. Bem mais que scripts de instalação,
esses arquivos servem também para um registro dos comandos executados
e ilustram o passo a passo para execução do projeto.

Algumas observações são que tivemos 2 máquinas virtuais disponíveis,
a terceira estava offline a maior parte do semestre. Uma das disponíveis
acabou apresentando uma falha de atualização (no pacote linux-modules-extra-5.4.0-77-generic)
e não foi possível usar o apt nessa máquina. Por isso, acabamos usando
somente a máquina 10.1.0.21 para instalar o projeto. Contudo, seria
possível distribuir o kafka e os serviços em várias máquinas com
algumas alterações simples.
