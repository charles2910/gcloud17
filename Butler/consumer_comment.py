import time
from pymongo import MongoClient
from pprint import pprint
import os
import datetime
from bson.regex import Regex
from dotenv import load_dotenv
from bson.objectid import ObjectId
load_dotenv()
from kafka import KafkaConsumer
import ast

mongoURL = os.getenv('MONGODB_URL')
client = MongoClient(mongoURL)

database = client['Pegasus']
bugCollection = database['bugtracker']
packageCollection = database['packages']


consumer = KafkaConsumer(bootstrap_servers='andromeda.lasdpc.icmc.usp.br:9021',
                            auto_offset_reset='earliest',
                            consumer_timeout_ms=1000)

consumer.subscribe('comments', 0)

count = 0
for message in consumer:
    if message is not None:
        number = message.offset
        value = message.value
        #print(value)
        count = count + 1
        print(count)
totalComments = count
print(totalComments)

print("waiting for comments...")

while(1):
    time.sleep(2)
    count2 = 0
    for message in consumer:
        if message is not None:
            number = message.offset
            value = message.value
            count2 = count2 + 1
            print(count2)

        if( count != 0 ):
            dict_str = value.decode("UTF-8")
            print(dict_str)
            mydata = ast.literal_eval(dict_str)
            print(repr(mydata))
            dictId = mydata['bugId']
            bugs = bugCollection.find_one({'bug-id':dictId})
            pprint(bugs)
            mydata.pop('bugId')
            filter = {'bug-id':dictId}
            comentarioNovo = { "$push":{"comments":mydata} }
            print(comentarioNovo)
            bugCollection.update_one(filter, comentarioNovo)
            print("Comment added: ")
            pprint(comentarioNovo)
            print("waiting for new comment")
            totalComments = count

