import time
from pymongo import MongoClient
import pymongo
from pprint import pprint
import os
import datetime
from bson.regex import Regex
from dotenv import load_dotenv
load_dotenv() 
from kafka import KafkaConsumer
import ast

mongoURL = os.getenv('MONGODB_URL')
client = MongoClient(mongoURL)

database = client['Pegasus']
bugCollection = database['bugtracker']
packageCollection = database['packages']

consumer = KafkaConsumer(bootstrap_servers='andromeda.lasdpc.icmc.usp.br:9021',
                            auto_offset_reset='earliest',
                            consumer_timeout_ms=1000)
#print(consumer.partitions_for_topic('bugs'))
#print(consumer.bootstrap_connected())
topics = consumer.topics()
#print(topics)
consumer.subscribe('bugs', 0)


last = bugCollection.find_one({},sort=[( '_id', pymongo.DESCENDING)])
lastPublish = last['bug-id']

print("waiting for post...")

while(1):
    time.sleep(0.02)
    count = 0
    for message in consumer:
        if message is not None:
            #print(message.offset, message.value)
            number = message.offset
            value = message.value
            count = count + 1
            print(count)

        if( number > lastPublish ):

            dict_str = value.decode("UTF-8")

            mydata = ast.literal_eval(dict_str)
            print(repr(mydata))

            mydata['bug-id'] = number
            lastPublish = number
            mydata['comments']=[]

            bugCollection.insert_one(mydata)
            print("Bug number ", number, " posted")
            print("Waiting for new bugs")

