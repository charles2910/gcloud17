# Butler


### Setup

Use pip3 to install the requirements

```terminal
pip3 install -r requirements.txt
```

### Fire it up

To make Butler start listening keep both following commands running on two terminals.

```terminal1
python3 ~/gcloud17/consumer_bug.py
```

```terminal2
python3 ~/gcloud17/consumer_comment.py
```

To automate this we used screen, which description will be on Liftman's directory's README.