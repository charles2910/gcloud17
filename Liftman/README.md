# Serviço Liftman

Esse serviço funciona basicamente como o endpoint da API usada para criar bugs
e adicionar comentários aos bugs. Ele não só é a interface, mas também o
responsável pela função de produtor de eventos para o kafka.

## API

A nossa API possui duas rotas principais:
- `/bugs`: lida com a criação de bugs através de requisições com o método POST
- `/bugs/<bugID>/comments`: lida com a adição de comentários em bugs específicos

Ambos as rotas aceitam somente o método POST porque a função desse serviço é
a criação de bugs e comentários.

### Na prática

Ao utilizar a nuvem para hospedar o serviço e consequentemente a API, nos
deparamos com o problema de hospedar mais de uma página/serviço em uma
máquina. Desta forma, foi necessário usar o NGINX para encaminhar as
requisições para os serviços corretos, e foi adicionado um prefixo
(`/liftman/`) ao caminho da API.

Assim, o acesso das ruas rotas se dá através dos caminhos:
- `/liftman/bugs`
- `/liftman/bugs/<bugId>/comments`

## Execução

Para executar o serviço, é necessário ter instaladas algumas bibliotecas. Foram
utilizadas as bibliotecas `kafka-python` e `flask` nesse serviços e um simples
`pip3 install -r requirements.txt` é suficiente para instalá-las.

Para executar o serviço, basta executar `python3 liftman.py` e a API estará
funcionando. Por padrão, ela estará escutando localmente na porta 5000 nas
rotas originais (i.e. sem o `/liftman/`).

## Autoria

Esse serviço foi feito por Carlos Henrique Lima Melara e está licenciado
sob os termos da GPL-3+.
