import json
import sys

from flask import Flask, request
from kafka import KafkaProducer

app = Flask(__name__)

# Rota da API para criação de novos bugs
@app.route('/bugs', methods=['POST'])
def Writer():
	bug = request.get_json()
	producer = KafkaProducer(bootstrap_servers='andromeda.lasdpc.icmc.usp.br:9021', value_serializer=lambda v: json.dumps(v).encode('utf-8'))
	producer.send('bugs', bug)
	producer.close()
	return bug

# Rota da API para adição de comentários em bugs
@app.route('/bugs/<bugId>/comments', methods=['POST'])
def Commenter(bugId):
	new_comment = request.get_json()
	producer = KafkaProducer(bootstrap_servers='andromeda.lasdpc.icmc.usp.br:9021', value_serializer=lambda v: json.dumps(v).encode('utf-8'))
	producer.send('comments', new_comment)
	producer.close()
	return new_comment

if __name__ == "__main__":
	app.run()
