# BugPortal

To install this service you need node 14.x and yarn installed

### Setup

We use Yarn as our package manager. To get the dependencies installed, just do this in the root directory:

```terminal
yarn install
```

### Build

To build the service use:

```terminal
yarn redwood build
```


### Fire it up

Finally to start the service use:

```terminal
yarn redwood serve
```
