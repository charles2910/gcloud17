import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes, navigate } from '@redwoodjs/router'

const DELETE_BUG_COMMENT_MUTATION = gql`
  mutation DeleteBugCommentMutation($id: Int!) {
    deleteBugComment(id: $id) {
      id
    }
  }
`

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const BugComment = ({ bugComment }) => {
  const [deleteBugComment] = useMutation(DELETE_BUG_COMMENT_MUTATION, {
    onCompleted: () => {
      toast.success('BugComment deleted')
      navigate(routes.bugComments())
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete bugComment ' + id + '?')) {
      deleteBugComment({ variables: { id } })
    }
  }

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            BugComment {bugComment.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{bugComment.id}</td>
            </tr>
            <tr>
              <th>From</th>
              <td>{bugComment.from}</td>
            </tr>
            <tr>
              <th>Date</th>
              <td>{bugComment.date}</td>
            </tr>
            <tr>
              <th>Text</th>
              <td>{bugComment.text}</td>
            </tr>
            <tr>
              <th>Bug id</th>
              <td>{bugComment.bugId}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        {/* <Link
          to={routes.editBugComment({ id: bugComment.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <a
          href="#"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(bugComment.id)}
        >
          Delete
        </a> */}
      </nav>
    </>
  )
}

export default BugComment
