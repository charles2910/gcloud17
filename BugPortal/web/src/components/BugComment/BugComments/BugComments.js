import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes } from '@redwoodjs/router'

import { QUERY } from 'src/components/BugComment/BugCommentsCell'

const DELETE_BUG_COMMENT_MUTATION = gql`
  mutation DeleteBugCommentMutation($id: Int!) {
    deleteBugComment(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const BugCommentsList = ({ bugComments }) => {
  const [deleteBugComment] = useMutation(DELETE_BUG_COMMENT_MUTATION, {
    onCompleted: () => {
      toast.success('BugComment deleted')
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete bugComment ' + id + '?')) {
      deleteBugComment({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>From</th>
            <th>Date</th>
            <th>Text</th>

            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {bugComments.map((bugComment) => (
            <tr key={bugComment.id}>
              <td>{truncate(bugComment.from ?? 'undefined')}</td>
              <td>
                {truncate(timeTag(Number(bugComment.date ?? 'undefined')))}
              </td>
              <td>{truncate(bugComment.text ?? 'undefined')}</td>

              <td>
                <nav className="rw-table-actions">
                  {/* <Link
                    to={routes.bugComment({ id: bugComment.id })}
                    title={'Show bugComment ' + bugComment.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  <Link
                    to={routes.editBugComment({ id: bugComment.id })}
                    title={'Edit bugComment ' + bugComment.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <a
                    href="#"
                    title={'Delete bugComment ' + bugComment.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(bugComment.id)}
                  >
                    Delete
                  </a> */}
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default BugCommentsList
