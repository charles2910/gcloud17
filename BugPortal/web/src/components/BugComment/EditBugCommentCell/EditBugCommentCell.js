import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { navigate, routes } from '@redwoodjs/router'
import BugCommentForm from 'src/components/BugComment/BugCommentForm'

export const QUERY = gql`
  query FindBugCommentById($id: Int!) {
    bugComment: bugComment(id: $id) {
      id
      from
      date
      text
      bugId
    }
  }
`
const UPDATE_BUG_COMMENT_MUTATION = gql`
  mutation UpdateBugCommentMutation($id: Int!, $input: UpdateBugCommentInput!) {
    updateBugComment(id: $id, input: $input) {
      id
      from
      date
      text
      bugId
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Success = ({ bugComment }) => {
  const [updateBugComment, { loading, error }] = useMutation(
    UPDATE_BUG_COMMENT_MUTATION,
    {
      onCompleted: () => {
        toast.success('BugComment updated')
        navigate(routes.bugComments())
      },
    }
  )

  const onSave = (input, id) => {
    const castInput = Object.assign(input, { bugId: parseInt(input.bugId) })
    updateBugComment({ variables: { id, input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">
          Edit BugComment {bugComment.id}
        </h2>
      </header>
      <div className="rw-segment-main">
        <BugCommentForm
          bugComment={bugComment}
          onSave={onSave}
          error={error}
          loading={loading}
        />
      </div>
    </div>
  )
}
