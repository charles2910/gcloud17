import BugComment from 'src/components/BugComment/BugComment'

export const QUERY = gql`
  query FindBugCommentById($id: Int!) {
    bugComment: bugComment(id: $id) {
      id
      from
      date
      text
      bugId
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>BugComment not found</div>

export const Success = ({ bugComment }) => {
  return <BugComment bugComment={bugComment} />
}
