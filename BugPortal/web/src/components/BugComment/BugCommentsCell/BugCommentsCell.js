import { Link, routes } from '@redwoodjs/router'

import BugComments from 'src/components/BugComment/BugComments'

export const QUERY = gql`
  query BUG_COMMENTS {
    bugComments {
      id
      from
      date
      text
      bugId
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No bugComments yet. '}
      <Link to={routes.newBugComment()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ bugComments }) => {
  return <BugComments bugComments={bugComments} />
}
