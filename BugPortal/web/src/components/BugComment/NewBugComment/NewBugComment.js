import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { navigate, routes } from '@redwoodjs/router'
import BugCommentForm from 'src/components/BugComment/BugCommentForm'

const CREATE_BUG_COMMENT_MUTATION = gql`
  mutation CreateBugCommentMutation($input: CreateBugCommentInput!) {
    createBugComment(input: $input) {
      id
    }
  }
`

const NewBugComment = ({ id }) => {
  const [createBugComment, { loading, error }] = useMutation(
    CREATE_BUG_COMMENT_MUTATION,
    {
      onCompleted: () => {
        toast.success('BugComment created')
        navigate(routes.bug({ id }))
      },
    }
  )

  const onSave = (input) => {
    const castInput = Object.assign(input, { bugId: parseInt(id) })
    createBugComment({ variables: { input: castInput } })
  }

  return (
    <div className="rw-segment">
      <header className="rw-segment-header">
        <h2 className="rw-heading rw-heading-secondary">New BugComment</h2>
      </header>
      <div className="rw-segment-main">
        <BugCommentForm onSave={onSave} loading={loading} error={error} />
      </div>
    </div>
  )
}

export default NewBugComment
