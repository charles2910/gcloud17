import Bug from 'src/components/Bug/Bug'

export const QUERY = gql`
  query FindBugById($id: Int!) {
    bug: bug(id: $id) {
      id
      title
      package
      from
      description
      severity
      tags
      date
      comments {
        from
        date
        text
      }
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => <div>Bug not found</div>

export const Success = ({ bug }) => {
  return <Bug bug={bug} />
}
