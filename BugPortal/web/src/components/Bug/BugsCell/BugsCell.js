import { Link, routes } from '@redwoodjs/router'

import Bugs from 'src/components/Bug/Bugs'

export const QUERY = gql`
  query BUGS {
    bugs {
      id
      title
      package
      from
      description
      severity
      tags
      date
    }
  }
`

export const Loading = () => <div>Loading...</div>

export const Empty = () => {
  return (
    <div className="rw-text-center">
      {'No bugs yet. '}
      <Link to={routes.newBug()} className="rw-link">
        {'Create one?'}
      </Link>
    </div>
  )
}

export const Success = ({ bugs }) => {
  return <Bugs bugs={bugs} />
}
