import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes } from '@redwoodjs/router'

import { QUERY } from 'src/components/Bug/BugsCell'

const DELETE_BUG_MUTATION = gql`
  mutation DeleteBugMutation($id: Int!) {
    deleteBug(id: $id) {
      id
    }
  }
`

const MAX_STRING_LENGTH = 150

const truncate = (text) => {
  let output = text
  if (text && text.length > MAX_STRING_LENGTH) {
    output = output.substring(0, MAX_STRING_LENGTH) + '...'
  }
  return output
}

const jsonTruncate = (obj) => {
  return truncate(JSON.stringify(obj, null, 2))
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const BugsList = ({ bugs }) => {
  const [deleteBug] = useMutation(DELETE_BUG_MUTATION, {
    onCompleted: () => {
      toast.success('Bug deleted')
    },
    // This refetches the query on the list page. Read more about other ways to
    // update the cache over here:
    // https://www.apollographql.com/docs/react/data/mutations/#making-all-other-cache-updates
    refetchQueries: [{ query: QUERY }],
    awaitRefetchQueries: true,
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete bug ' + id + '?')) {
      deleteBug({ variables: { id } })
    }
  }

  return (
    <div className="rw-segment rw-table-wrapper-responsive">
      <table className="rw-table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Package</th>
            <th>From</th>
            <th>Description</th>
            <th>Severity</th>
            <th>Tags</th>
            <th>Date</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {bugs.map((bug) => (
            <tr key={bug.id}>
              <td>{truncate(bug.id)}</td>
              <td>{truncate(bug.title)}</td>
              <td>{truncate(bug.package)}</td>
              <td>{truncate(bug.from)}</td>
              <td>{truncate(bug.description)}</td>
              <td>{truncate(bug.severity)}</td>
              <td>{truncate(bug.tags)}</td>
              <td>{truncate(timeTag(Number(bug.date)))}</td>
              <td>
                <nav className="rw-table-actions">
                  <Link
                    to={routes.bug({ id: bug.id })}
                    title={'Show bug ' + bug.id + ' detail'}
                    className="rw-button rw-button-small"
                  >
                    Show
                  </Link>
                  {/* <Link
                    to={routes.editBug({ id: bug.id })}
                    title={'Edit bug ' + bug.id}
                    className="rw-button rw-button-small rw-button-blue"
                  >
                    Edit
                  </Link>
                  <a
                    href="#"
                    title={'Delete bug ' + bug.id}
                    className="rw-button rw-button-small rw-button-red"
                    onClick={() => onDeleteClick(bug.id)}
                  >
                    Delete
                  </a> */}
                </nav>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default BugsList
