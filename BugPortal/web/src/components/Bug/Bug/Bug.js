import { useMutation } from '@redwoodjs/web'
import { toast } from '@redwoodjs/web/toast'
import { Link, routes, navigate } from '@redwoodjs/router'
import BugComments from 'src/components/BugComment/BugComments'

const DELETE_BUG_MUTATION = gql`
  mutation DeleteBugMutation($id: Int!) {
    deleteBug(id: $id) {
      id
    }
  }
`

const jsonDisplay = (obj) => {
  return (
    <pre>
      <code>{JSON.stringify(obj, null, 2)}</code>
    </pre>
  )
}

const timeTag = (datetime) => {
  return (
    <time dateTime={datetime} title={datetime}>
      {new Date(datetime).toUTCString()}
    </time>
  )
}

const checkboxInputTag = (checked) => {
  return <input type="checkbox" checked={checked} disabled />
}

const Bug = ({ bug }) => {
  const [deleteBug] = useMutation(DELETE_BUG_MUTATION, {
    onCompleted: () => {
      toast.success('Bug deleted')
      navigate(routes.bugs())
    },
  })

  const onDeleteClick = (id) => {
    if (confirm('Are you sure you want to delete bug ' + id + '?')) {
      deleteBug({ variables: { id } })
    }
  }
  console.log('bugcomment', bug.comments)

  return (
    <>
      <div className="rw-segment">
        <header className="rw-segment-header">
          <h2 className="rw-heading rw-heading-secondary">
            Bug {bug.id} Detail
          </h2>
        </header>
        <table className="rw-table">
          <tbody>
            <tr>
              <th>Id</th>
              <td>{bug.id}</td>
            </tr>
            <tr>
              <th>Title</th>
              <td>{bug.title}</td>
            </tr>
            <tr>
              <th>Package</th>
              <td>{bug.package}</td>
            </tr>
            <tr>
              <th>From</th>
              <td>{bug.from}</td>
            </tr>
            <tr>
              <th>Description</th>
              <td>{bug.description}</td>
            </tr>
            <tr>
              <th>Severity</th>
              <td>{bug.severity}</td>
            </tr>
            <tr>
              <th>Tags</th>
              <td>{bug.tags}</td>
            </tr>
            <tr>
              <th>Date</th>
              <td>{timeTag(Number(bug.date))}</td>
            </tr>
          </tbody>
        </table>
      </div>
      <nav className="rw-button-group">
        {/* <Link
          to={routes.editBug({ id: bug.id })}
          className="rw-button rw-button-blue"
        >
          Edit
        </Link>
        <a
          href="#"
          className="rw-button rw-button-red"
          onClick={() => onDeleteClick(bug.id)}
        >
          Delete
        </a> */}
      </nav>

      <h2 className="rw-heading rw-heading-secondary">Comments</h2>
      <br></br>
      <Link
        to={routes.newBugComment({ id: bug.id })}
        className="rw-button rw-button-green"
      >
        <div className="rw-button-icon">+</div> New Bug Comment
      </Link>
      <BugComments bugComments={bug.comments}></BugComments>
    </>
  )
}

export default Bug
