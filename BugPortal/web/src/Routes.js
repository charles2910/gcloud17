// In this file, all Page components from 'src/pages` are auto-imported. Nested
// directories are supported, and should be uppercase. Each subdirectory will be
// prepended onto the component name.
//
// Examples:
//
// 'src/pages/HomePage/HomePage.js'         -> HomePage
// 'src/pages/Admin/BooksPage/BooksPage.js' -> AdminBooksPage

import { Set, Router, Route } from '@redwoodjs/router'
import BugCommentsLayout from 'src/layouts/BugCommentsLayout'
import BugsLayout from 'src/layouts/BugsLayout'

const Routes = () => {
  return (
    <Router>
      <Set wrap={BugCommentsLayout}>
        <Route path="/bug-comments/{id:Int}/new" page={BugCommentNewBugCommentPage} name="newBugComment" />
        <Route path="/bug-comments/{id:Int}/edit" page={BugCommentEditBugCommentPage} name="editBugComment" />
        <Route path="/bug-comments/{id:Int}" page={BugCommentBugCommentPage} name="bugComment" />
      </Set>
      <Set wrap={BugsLayout}>
        <Route path="/" page={BugBugsPage} name="home" />
        <Route path="/bugs/new" page={BugNewBugPage} name="newBug" />
        <Route path="/bugs/{id:Int}/edit" page={BugEditBugPage} name="editBug" />
        <Route path="/bugs/{id:Int}" page={BugBugPage} name="bug" />
        <Route path="/bugs" page={BugBugsPage} name="bugs" />
      </Set>
      <Route notfound page={NotFoundPage} />
    </Router>
  )
}

export default Routes
