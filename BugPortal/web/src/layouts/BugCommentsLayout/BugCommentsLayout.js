import { Toaster } from '@redwoodjs/web/toast'

const BugCommentsLayout = (props) => {
  return (
    <div className="rw-scaffold">
      <Toaster />

      <main className="rw-main">{props.children}</main>
    </div>
  )
}

export default BugCommentsLayout
