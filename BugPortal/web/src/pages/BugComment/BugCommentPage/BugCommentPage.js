import BugCommentCell from 'src/components/BugComment/BugCommentCell'

const BugCommentPage = ({ id }) => {
  return <BugCommentCell id={id} />
}

export default BugCommentPage
