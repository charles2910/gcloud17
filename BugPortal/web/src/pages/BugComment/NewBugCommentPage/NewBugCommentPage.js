import NewBugComment from 'src/components/BugComment/NewBugComment'

const NewBugCommentPage = ({ id }) => {
  return <NewBugComment id={id} />
}

export default NewBugCommentPage
