import EditBugCommentCell from 'src/components/BugComment/EditBugCommentCell'

const EditBugCommentPage = ({ id }) => {
  return <EditBugCommentCell id={id} />
}

export default EditBugCommentPage
