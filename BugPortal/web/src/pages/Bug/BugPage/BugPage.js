import BugCell from 'src/components/Bug/BugCell'

const BugPage = ({ id }) => {
  return <BugCell id={id} />
}

export default BugPage
