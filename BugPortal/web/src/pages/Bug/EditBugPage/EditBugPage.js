import EditBugCell from 'src/components/Bug/EditBugCell'

const EditBugPage = ({ id }) => {
  return <EditBugCell id={id} />
}

export default EditBugPage
