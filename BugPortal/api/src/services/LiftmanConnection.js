import fetch from 'node-fetch'

const liftManUrl = 'http://andromeda.lasdpc.icmc.usp.br:9022/liftman'

const PostBugUrl = liftManUrl + '/bugs'

const PostCommentUrl = (bugId) => liftManUrl + '/bugs/' + bugId + '/comments'

export async function createNewBug(bugData) {
  return fetch(PostBugUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(bugData),
  })
}

export async function createNewComment(bugId, commentData) {
  return fetch(PostCommentUrl(bugId), {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(commentData),
  })
}
