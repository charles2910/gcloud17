import { db } from 'src/lib/db'
import { requireAuth } from 'src/lib/auth'
import { getBugTrackerCollection } from '../MongoDb'

import { createNewBug } from '../LiftmanConnection'

function formatBug(bug) {
  return {
    ...bug,
    id: bug['bug-id'],
    tags: bug.tags.reduce((sum, elem) => sum + ' , ' + elem),
  }
}

// Used when the environment variable REDWOOD_SECURE_SERVICES=1
export const beforeResolver = (rules) => {
  rules.add(requireAuth)
}

export const bugs = async () => {
  const bugs = await getBugTrackerCollection().find().toArray()

  return bugs.map((bug) => formatBug(bug))
}

export const bug = async ({ id }) => {
  const bug = await getBugTrackerCollection().findOne({ ['bug-id']: id })

  return formatBug(bug)
}

function convertTagToArray(tags) {
  return tags.split(',')
}

export const createBug = async ({ input }) => {
  console.log('creating bug', input)

  const bugData = { ...input, tags: convertTagToArray(input.tags) }

  const response = await createNewBug(bugData)

  console.log(response)
  console.log(await response.json())

  return { id: 0, input }
}
export const updateBug = ({ id, input }) => {
  return db.bug.update({
    data: input,
    where: { id },
  })
}

export const deleteBug = ({ id }) => {
  return db.bug.delete({
    where: { id },
  })
}

export const Bug = {
  comments: (_obj, { root }) => root.comments,
}
