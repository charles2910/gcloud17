export const standard = defineScenario({
  bug: {
    one: {
      title: 'String',
      package: 'String',
      from: 'String',
      description: 'String',
      severity: 'String',
      tags: 'String',
      date: 'String',
    },

    two: {
      title: 'String',
      package: 'String',
      from: 'String',
      description: 'String',
      severity: 'String',
      tags: 'String',
      date: 'String',
    },
  },
})
