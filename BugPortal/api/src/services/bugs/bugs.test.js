import { bugs, bug, createBug, updateBug, deleteBug } from './bugs'

describe('bugs', () => {
  scenario('returns all bugs', async (scenario) => {
    const result = await bugs()

    expect(result.length).toEqual(Object.keys(scenario.bug).length)
  })

  scenario('returns a single bug', async (scenario) => {
    const result = await bug({ id: scenario.bug.one.id })

    expect(result).toEqual(scenario.bug.one)
  })

  scenario('creates a bug', async () => {
    const result = await createBug({
      input: {
        title: 'String',
        package: 'String',
        from: 'String',
        description: 'String',
        severity: 'String',
        tags: 'String',
        date: 'String',
      },
    })

    expect(result.title).toEqual('String')
    expect(result.package).toEqual('String')
    expect(result.from).toEqual('String')
    expect(result.description).toEqual('String')
    expect(result.severity).toEqual('String')
    expect(result.tags).toEqual('String')
    expect(result.date).toEqual('String')
  })

  scenario('updates a bug', async (scenario) => {
    const original = await bug({ id: scenario.bug.one.id })
    const result = await updateBug({
      id: original.id,
      input: { title: 'String2' },
    })

    expect(result.title).toEqual('String2')
  })

  scenario('deletes a bug', async (scenario) => {
    const original = await deleteBug({ id: scenario.bug.one.id })
    const result = await bug({ id: original.id })

    expect(result).toEqual(null)
  })
})
