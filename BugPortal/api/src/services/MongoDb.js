import { MongoClient } from 'mongodb'

let db = null

async function ConnectToMongo() {
  const uri =
    'mongodb+srv://admin:admin@clustergc-sp.6jn3r.mongodb.net/Pegasus?retryWrites=true&w=majority'
  const client = new MongoClient(uri)
  await client.connect()

  // const databasesList = await client.db('Pegasus').listCollections().toArray()
  // console.log(databasesList)

  db = client.db('Pegasus')
}

ConnectToMongo()

export function getBugTrackerCollection() {
  return db.collection('bugtracker')

  //const bugs = await collection.find({}).toArray()
  //console.log(bugs)
}

export function getPackagesCollection() {
  return db.collection('packages')
}
