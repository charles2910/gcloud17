import { db } from 'src/lib/db'
import { requireAuth } from 'src/lib/auth'

import { createNewComment } from '../LiftmanConnection'

// Used when the environment variable REDWOOD_SECURE_SERVICES=1
export const beforeResolver = (rules) => {
  rules.add(requireAuth)
}

export const bugComments = () => {
  return db.bugComment.findMany()
}

export const bugComment = ({ id }) => {
  return db.bugComment.findUnique({
    where: { id },
  })
}

export const createBugComment = async ({ input }) => {
  console.log('input', input)
  const response = await createNewComment(input.bugId, input)

  console.log(response)
  console.log(await response.json())

  return { id: input.bugId, ...input }
}

export const updateBugComment = ({ id, input }) => {
  return db.bugComment.update({
    data: input,
    where: { id },
  })
}

export const deleteBugComment = ({ id }) => {
  return db.bugComment.delete({
    where: { id },
  })
}

export const BugComment = {
  Bug: (_obj, { root }) =>
    db.bugComment.findUnique({ where: { id: root.id } }).Bug(),
}
