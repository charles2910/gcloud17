import {
  bugComments,
  bugComment,
  createBugComment,
  updateBugComment,
  deleteBugComment,
} from './bugComments'

describe('bugComments', () => {
  scenario('returns all bugComments', async (scenario) => {
    const result = await bugComments()

    expect(result.length).toEqual(Object.keys(scenario.bugComment).length)
  })

  scenario('returns a single bugComment', async (scenario) => {
    const result = await bugComment({ id: scenario.bugComment.one.id })

    expect(result).toEqual(scenario.bugComment.one)
  })

  scenario('creates a bugComment', async () => {
    const result = await createBugComment({
      input: { from: 'String', date: 'String', text: 'String' },
    })

    expect(result.from).toEqual('String')
    expect(result.date).toEqual('String')
    expect(result.text).toEqual('String')
  })

  scenario('updates a bugComment', async (scenario) => {
    const original = await bugComment({ id: scenario.bugComment.one.id })
    const result = await updateBugComment({
      id: original.id,
      input: { from: 'String2' },
    })

    expect(result.from).toEqual('String2')
  })

  scenario('deletes a bugComment', async (scenario) => {
    const original = await deleteBugComment({ id: scenario.bugComment.one.id })
    const result = await bugComment({ id: original.id })

    expect(result).toEqual(null)
  })
})
