export const schema = gql`
  type BugComment {
    id: Int!
    from: String!
    date: String!
    text: String!
    Bug: Bug
    bugId: Int
  }

  type Query {
    bugComments: [BugComment!]!
    bugComment(id: Int!): BugComment
  }

  input CreateBugCommentInput {
    from: String!
    date: String!
    text: String!
    bugId: Int
  }

  input UpdateBugCommentInput {
    from: String
    date: String
    text: String
    bugId: Int
  }

  type Mutation {
    createBugComment(input: CreateBugCommentInput!): BugComment!
    updateBugComment(id: Int!, input: UpdateBugCommentInput!): BugComment!
    deleteBugComment(id: Int!): BugComment!
  }
`
