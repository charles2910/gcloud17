export const schema = gql`
  type CommentInBug {
    id: Int!
    from: String!
    date: String!
    text: String!
    Bug: Bug
    bugId: Int
  }

  type Bug {
    id: Int!
    title: String!
    package: String!
    from: String!
    description: String!
    severity: String!
    tags: String!
    date: String!
    comments: [CommentInBug]!
  }

  type Query {
    bugs: [Bug!]!
    bug(id: Int!): Bug
  }

  input CreateBugInput {
    title: String!
    package: String!
    from: String!
    description: String!
    severity: String!
    tags: String!
    date: String!
  }

  input UpdateBugInput {
    title: String
    package: String
    from: String
    description: String
    severity: String
    tags: String
    date: String
  }

  type Mutation {
    createBug(input: CreateBugInput!): Bug!
    updateBug(id: Int!, input: UpdateBugInput!): Bug!
    deleteBug(id: Int!): Bug!
  }
`
