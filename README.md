# Projeto da disciplina Computação em Nuvem e Arquitetura Orientadas a Serviços

Grupo 17 - Carlos, Daniel e Lucas

## Introdução

Este projeto tem como objetivo criar uma infraestrutura para gerenciar
defeitos de software disponibilizados em pacotes para a distribuição
GNU/Linux Debian.

O Debian disponibiliza mais de 60.000 pacotes (aplicações) em seus
repositórios oficiais. Por isso, é necessário haver uma infraestrutura
para gerencciar defeitos e outros problemas encontrados por quem usa
as aplicações.

Atualmente o Debian possui uma infraestrutura, mas ela é bastante
antiga e antiquado (contudo funciona muito bem), utilizando basicamente
comunicações via email para reportar, responder e gerenciar defeitos.
Assim, nosso grupo decidiu usar este ecossistema para fazer um estudo de
caso e projetar uma infraestrutura modernizada para o Debian, baseada em
troca de mensagens e utilizando uma API RESTful como interface.
